import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: chenbin
 * Date: 2/17/15
 * Time: 9:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class Trie {

    private static final int ALPHABET_SIZE = 26;

    public static final class TrieNode {
        private char character;
        private boolean isWord = false;
        private TrieNode[] children= null;

        public TrieNode(char ch) {
           character = ch;
        }

        public TrieNode getOrCreateChild(char ch) {
            if(children == null) {
                children = new TrieNode[ALPHABET_SIZE];
            }
            TrieNode child = children[getIndex(ch)];
            if(child == null) {
                child = new TrieNode(ch);
                children[getIndex(ch)] = child;
            }
            return child;
        }

        public TrieNode get(char ch) {
            return children != null ? children[getIndex(ch)] : null;
        }

        private int getIndex(char ch) {
//            String alphabet = "abcdefghijklmnopqrstuvwxyz";
//            return alphabet.indexOf(ch);
            return Character.getNumericValue(ch) - Character.getNumericValue('a');
        }

        public static int getAlphabetSize() {
            return ALPHABET_SIZE;
        }

        public char getCharacter() {
            return character;
        }

        public void setCharacter(char character) {
            this.character = character;
        }

        public boolean isWord() {
            return isWord;
        }

        public void setWord(boolean word) {
            isWord = word;
        }

        public TrieNode[] getChildren() {
            return children;
        }

        public void setChildren(TrieNode[] children) {
            this.children = children;
        }
    }

    private TrieNode root;
    private int size = 0;

    public Trie() {
        root = new TrieNode((char)0);
    }

    public boolean add(String word){
        if (contains(word)) {
            return false;
        }
        TrieNode node = root;
        for(char ch : word.toLowerCase().toCharArray()) {
            node = node.getOrCreateChild(ch);
        }
        node.setWord(true);
        size ++;
        return true;
    }

    public boolean contains(String word) {
        TrieNode node = findTrieNodeByPrefix(word);
        return node != null && node.isWord();
    }

    public List<String> wordList() {
        List<String> result = new ArrayList<String>(size);
        StringBuilder sb = new StringBuilder();
        wordList(root, result, sb);
        return result;
    }

    private void wordList(TrieNode node, List<String> result, StringBuilder sb) {
        if (node == null) {
            return;
        }
        if (node.isWord()) {
            result.add(sb.toString());
        }
        for(TrieNode child : node.getChildren()) {
            sb.append(child.getCharacter());
            wordList(child, result, sb);
            sb.deleteCharAt(sb.length()-1);
        }
    }

    public List<String> wordList(String prefix, boolean inclusive) {
        List<String> result = new ArrayList<String>();
        TrieNode node = findTrieNodeByPrefix(prefix);
        if(node == null) {
            return result;
        }
        StringBuilder sb = new StringBuilder(prefix);
        wordList(node, result, sb);
        if(!inclusive) {
            result.remove(prefix);
        }
        return result;
    }

    public boolean isPrefix(String prefix) {
        TrieNode node = findTrieNodeByPrefix(prefix);
        return node != null && node.children != null;
    }

    private TrieNode findTrieNodeByPrefix(String prefix) {
        TrieNode node = root;
        for(char ch : prefix.toLowerCase().toCharArray()) {
            node = node.get(ch);
            if(node == null) {
                break;
            }
        }
        return node;
    }
}
